export const environment = {
  production: true,
  wsEndpoint: "https://api.heroku.com/customapp/api",
  dummyUsers: [
    {
      email: "pwa@admin.com",
      password: "pwapwa1234"
    },
    {
      email: "admin@admin.com",
      password: "pwapwa1234"
    }
  ]
};
