import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Constants } from '../common/constants';
import { LoginTo } from '../interfaces/login-to';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  userSubject = new BehaviorSubject<any>({});

  constructor() { }

  login(loginTo: LoginTo): any {
    if (environment.production) {
      //chiama il servizio rest per autenticare l'utente
      return false;
    } else {
      let dummyUser = environment.dummyUsers.find((dummyUser) => {
        return (loginTo.email === dummyUser.email && loginTo.password === dummyUser.password);
      });
      if (dummyUser) {
        localStorage.setItem(Constants.LOCALSTORAGE_KEY_LOGGED_IN, "true");
        localStorage.setItem(Constants.LOCALSTORAGE_KEY_USER, JSON.stringify(dummyUser));
        localStorage.setItem(Constants.LOCALSTORAGE_KEY_WHEN_LOGGED_IN, new Date().getTime().toString());
        this.userSubject.next(dummyUser);
        return true;
      }
      return false;
    }
  }

  logout() {
    localStorage.removeItem(Constants.LOCALSTORAGE_KEY_LOGGED_IN);
    localStorage.removeItem(Constants.LOCALSTORAGE_KEY_USER);
    this.userSubject.next(null);
  }

  isLoggedIn(): boolean {
    if (localStorage.getItem(Constants.LOCALSTORAGE_KEY_LOGGED_IN)) {
      return true;
    }
    return false;
  }

  getWhenLoggedIn() {
    return localStorage.getItem(Constants.LOCALSTORAGE_KEY_WHEN_LOGGED_IN);
  }

  getUser() {
    let userString = localStorage.getItem(Constants.LOCALSTORAGE_KEY_USER) as string;
    return JSON.parse(userString);
  }
}