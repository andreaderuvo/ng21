import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Post } from '../interfaces/post';
import { EntityService } from './entity.service';

@Injectable({
  providedIn: 'root'
})
export class PostService extends EntityService<Post> {

  constructor(protected httpClient: HttpClient) {
    super(httpClient);
  }

  get entityName(): string {
    return "posts";
  }

}
