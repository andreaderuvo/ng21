import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Todo } from '../interfaces/todo';
import { EntityService } from './entity.service';

@Injectable({
  providedIn: 'root'
})
export class TodoService extends EntityService<Todo> {

  constructor(protected httpClient: HttpClient) {
    super(httpClient);
  }

  get entityName(): string {
    return "todos";
  }

}
