export class Constants {
    static LOCALSTORAGE_KEY_LOGGED_IN = "loggedIn";
    static LOCALSTORAGE_KEY_USER = "user";
    static LOCALSTORAGE_KEY_WHEN_LOGGED_IN = "whenLoggedIn";
}
