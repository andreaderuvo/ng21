import { AbstractControl } from '@angular/forms';

export class CustomValidators {
    private static checkPwd(str: string) {
        let errorCode = "";
        if (str.length < 6) {
            errorCode = "too_short";
        } else if (str.length > 50) {
            errorCode = "too_long";
        } else if (str.search(/\d/) == -1) {
            errorCode = "no_num";
        } else if (str.search(/[a-zA-Z]/) == -1) {
            errorCode = "no_letter";
        }
        if (!errorCode) {
            return null;
        } else {
            return {
                [errorCode]: str
            }
        }
    }

    private static checkPhoneNumber(phoneNumber: string) {
        //todo
        return null;
    }

    static weakPassword(control: AbstractControl): any | null {
        return ((control: AbstractControl): { [key: string]: any } | null => {
            return CustomValidators.checkPwd(control.value);
        })(control);
    }

    static phoneNumber(control: AbstractControl): any | null {
        return ((control: AbstractControl): { [key: string]: any } | null => {
            return CustomValidators.phoneNumber(control.value);
        })(control);
    }
}