import { Entity } from './entity';

export interface Todo extends Entity {
    title: string,
    completed: boolean
}