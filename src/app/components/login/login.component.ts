import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginTo } from 'src/app/interfaces/login-to';
import { LoginService } from 'src/app/services/login.service';
import { PageComponent } from '../page/page.component';
import { UnauthorizedComponent } from '../unauthorized/unauthorized.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent extends PageComponent implements OnInit {

  loginForm: FormGroup;

  @ViewChild(UnauthorizedComponent) unauthorizedComponent: any;

  constructor(private loginService: LoginService, private router: Router) {
    super();

    this.loginForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required, Validators.minLength(8)])
    });
  }

  ngOnInit(): void {
  }

  get title(): string {
    return "Login Page";
  }

  submit() {

    let loginTo: LoginTo = {
      email: this.loginForm.value.email,
      password: this.loginForm.value.password
    }

    if (this.loginService.login(loginTo)) {
      this.router.navigate(['/todos']);
    } else {
      this.unauthorizedComponent.show();
      //this.router.navigate(['/401']);
    }

    return false;
  }

  onUnauthorizedEvent(event: string) {
    console.log(event);
  }
}
