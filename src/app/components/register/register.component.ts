import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { CustomValidators } from 'src/app/validators/custom_validators';
import { PageComponent } from '../page/page.component';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent extends PageComponent implements OnInit {

  isValidFormSubmitted: boolean = false;
  registerForm: FormGroup;
  //https://stackoverflow.com/questions/4338267/validate-phone-number-with-javascript
  regexPhoneNumber: string = "^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$";

  constructor() {
    super();

    this.registerForm = new FormGroup({
      firstName: new FormControl('', [Validators.required]),
      lastName: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required, CustomValidators.weakPassword]),
      address: new FormGroup({
        street: new FormControl(''),
        city: new FormControl(''),
        state: new FormControl(''),
        zip: new FormControl('')
      }),
      phoneNumbers: new FormArray([new FormControl('', [Validators.pattern(this.regexPhoneNumber)])])
    });
  }

  ngOnInit(): void {
  }

  get title(): string {
    return "Register Page";
  }

  get phoneNumbers() {
    return this.registerForm.get('phoneNumbers') as FormArray;
  }

  addPhoneNumber() {
    this.phoneNumbers.push(new FormControl('', [Validators.pattern(this.regexPhoneNumber)]));
  }

  submit() {
    if (this.registerForm.valid) {
      this.isValidFormSubmitted = true;
    }
  }
}
