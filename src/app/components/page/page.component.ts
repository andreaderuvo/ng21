import { Component, OnInit } from '@angular/core';

@Component({
  template: ""
})
export abstract class PageComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  abstract get title(): string;

}
