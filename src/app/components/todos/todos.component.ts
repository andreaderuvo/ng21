import { Component, OnInit, ViewChild } from '@angular/core';
import { Todo } from 'src/app/interfaces/todo';
import { TodoService } from 'src/app/services/todo.service';
import { LoadingComponent } from '../loading/loading.component';
import { PageComponent } from '../page/page.component';

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.css']
})
export class TodosComponent extends PageComponent implements OnInit {

  todos: Array<Todo>;

  @ViewChild(LoadingComponent) loadingComponent: any;

  constructor(private todoService: TodoService) {
    super();
    this.todos = [];
  }

  ngOnInit(): void {
    this.todoService.read().subscribe(todos => {
      this.todos = todos;
      this.loadingComponent.hide();
    });
  }

  get title(): string {
    return "Todos Page";
  }

}
