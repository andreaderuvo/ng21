import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/services/login.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  user: any;

  constructor(private router: Router, private loginService: LoginService) { }

  ngOnInit(): void {
    this.loginService.userSubject.subscribe((user) => {
      this.user = user;
    });

    this.user = this.loginService.getUser();
  }

  logout() {
    this.loginService.logout();
    this.router.navigate(['']);
  }

  get whenLoggedIn() {
    return this.loginService.getWhenLoggedIn();
  }
}
