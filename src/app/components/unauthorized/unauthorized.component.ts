import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-unauthorized',
  templateUrl: './unauthorized.component.html',
  styleUrls: ['./unauthorized.component.css']
})
export class UnauthorizedComponent implements OnInit {

  isVisible: boolean = false;
  
  @Output() onUnauthorizedEvent = new EventEmitter<string>();

  constructor() { }

  ngOnInit(): void {
  }

  show() {
    this.isVisible = true;
    this.onUnauthorizedEvent.emit("showEvent");
  }

  hide() {
    this.isVisible = false;
    this.onUnauthorizedEvent.emit("hideEvent");
  }

}
