import { Directive, Input, OnInit, TemplateRef, ViewContainerRef } from '@angular/core';
import { LoginService } from '../services/login.service';

@Directive({
  selector: '[appIsLoggedIn]'
})
export class IsLoggedInDirective implements OnInit {

  @Input("appIsLoggedIn") condition: boolean | undefined;

  constructor(private loginService: LoginService,
    private templateRef: TemplateRef<any>, private viewContainerRef: ViewContainerRef) {

    this.loginService.userSubject.subscribe((user) => {
      this.refreshView();
    });
  }

  ngOnInit(): void {
    this.refreshView();
  }

  refreshView() {
    this.viewContainerRef.clear();
    if (this.loginService.isLoggedIn() == this.condition) {
      this.viewContainerRef.createEmbeddedView(this.templateRef);
    } else {
      this.viewContainerRef.clear();
    }
  }

}
