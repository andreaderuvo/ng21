import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AboutComponent } from './components/about/about.component';
import { LoginComponent } from './components/login/login.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { RegisterComponent } from './components/register/register.component';
import { TodosComponent } from './components/todos/todos.component';
import { UnauthorizedComponent } from './components/unauthorized/unauthorized.component';
import { WelcomeComponent } from './components/welcome/welcome.component';
import { IsLoggedInGuard } from './guards/is-logged-in.guard';

const routes: Routes = [{
  path: '',
  component: WelcomeComponent
}, {
  path: 'welcome',
  component: WelcomeComponent
}, {
  path: 'about',
  component: AboutComponent
}, {
  path: 'login',
  component: LoginComponent
}, {
  path: 'register',
  component: RegisterComponent
}, {
  path: 'todos',
  component: TodosComponent,
  canActivate: [IsLoggedInGuard]
}, {
  path: '404',
  component: NotFoundComponent
}, {
  path: '**',
  redirectTo: '/404',
  pathMatch: 'full'
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
