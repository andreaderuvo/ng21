import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';

//COMPONENTS
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { WelcomeComponent } from './components/welcome/welcome.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { AboutComponent } from './components/about/about.component';
import { PageTitleComponent } from './components/page-title/page-title.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { TodosComponent } from './components/todos/todos.component';
import { UnauthorizedComponent } from './components/unauthorized/unauthorized.component';
import { LoadingComponent } from './components/loading/loading.component';

//DIRECTIVES
import { IsLoggedInDirective } from './directives/is-logged-in.directive';
import { AlternateCasePipe } from './pipes/alternate-case.pipe';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    WelcomeComponent,
    NotFoundComponent,
    AboutComponent,
    PageTitleComponent,
    LoginComponent,
    RegisterComponent,
    TodosComponent,
    UnauthorizedComponent,
    LoadingComponent,
    IsLoggedInDirective,
    AlternateCasePipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
