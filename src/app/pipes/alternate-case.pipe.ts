import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'alternate'
})
export class AlternateCasePipe implements PipeTransform {

  private alternateCase(value: string | undefined) {
    if (!value) value = "";
    var chars = value.toLowerCase().split("");
    for (var i = 0; i < chars.length; i += 2) {
      chars[i] = chars[i].toUpperCase();
    }
    return chars.join("");
  }

  transform(value: string | undefined, ...args: string[]): unknown {
    return this.alternateCase(value);
  }

}
